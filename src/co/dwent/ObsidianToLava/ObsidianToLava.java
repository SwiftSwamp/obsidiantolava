package co.dwent.ObsidianToLava;

import java.util.logging.Logger;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class ObsidianToLava extends JavaPlugin
    implements Listener
{
	public Logger pluginLog = Logger.getLogger("Minecraft");
	
	public void onEnable()
	{
		this.pluginLog.info("[ObsidianToLava v" + getDescription().getVersion() + "] has been enabled!");
		
		getServer().getPluginManager().registerEvents(this, this);
	}
		public void onDisable()
		{
			this.pluginLog.info("ObsidianToLava has been disabled!");
		}
		
		@EventHandler
		public void onPlayerInteract(PlayerInteractEvent event)
		{
			if ((event.getAction() == Action.RIGHT_CLICK_BLOCK) && (event.getClickedBlock().getTypeId() == 49))
			{
				Player player = event.getPlayer();
				if (!isSurroundedByWater(event.getClickedBlock()))
				{
					event.getClickedBlock().setTypeId(10);
					player.sendMessage(ChatColor.GREEN + "You have turned your obsididan back into Lava!");
				} else {
					player.sendMessage(ChatColor.DARK_RED + "You cannot do this next to water!");
				}
			}
		}
		
		private static boolean isSurroundedByWater(Block block) {
			for (BlockFace face : new BlockFace[] { BlockFace.NORTH, BlockFace.WEST, BlockFace.EAST, BlockFace.SOUTH, BlockFace.UP })
			{
				int type = block.getRelative(face).getTypeId();
				if ((type == 8) || (type == 9))
					return true;
			}
			return false;
		}
	}
